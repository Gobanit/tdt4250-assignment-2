package com.randakm.ntnu.tdt4250.uct.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.*;
import javax.servlet.http.*;

import org.osgi.service.component.annotations.*;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardServletPattern;

import com.randakm.ntnu.tdt4250.uct.api.Unit;
import com.randakm.ntnu.tdt4250.uct.api.UnitConversionService;

@Component
@HttpWhiteboardServletPattern("/convert/*")
public class UnitConvertServlet extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;
	
	@Reference(
			cardinality = ReferenceCardinality.MANDATORY,
			policy = ReferencePolicy.DYNAMIC
	)
	private volatile UnitConversionService conversionService;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String source = request.getParameter("sourceUnit");
		String target = request.getParameter("targetUnit");
		BigDecimal amount = new BigDecimal(request.getParameter("amount"));
		
		PrintWriter pw = response.getWriter();
		pw.write("Convert "+amount+" of "+source+" to "+target+"\n");
		pw.write("ServiceUsed: "+conversionService+"\n");
		
		Unit s = new Unit(source);
		Unit t = new Unit(target);
		
		BigDecimal bd = conversionService.convert(s, t, amount);
		pw.write("Result: "+bd+"\n");
	}

	
	public UnitConversionService getConversionService() {
		return conversionService;
	}

	public void setConversionService(UnitConversionService conversionService) {
		this.conversionService = conversionService;
	}
	
	
}