package com.randakm.ntnu.tdt4250.uct.servlet;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

public class ServletTest {

	@Test
	public void test() throws IOException {
		String httpPort = System.getProperty("org.osgi.service.http.port", "8080");
		URL url = new URL("http://localhost:" + httpPort + "/convert/" + "?sourceUnit=C&targetUnit=F&amount=6");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		Assert.assertEquals(200, con.getResponseCode());
	}

}
