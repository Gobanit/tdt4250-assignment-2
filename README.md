# TDT4250 Assignment 2

Assignment 2 for [TDT4250](https://www.ntnu.no/studier/emner/TDT4250) course at NTNU.
Repository contains multiple bundles. 

## Structure

### UnitConversionTool-api 
Contains basic interfaces or classes used in different bundles like UnitConvertor and UnitConvertingService interface.

### UnitConversionTool-console
Constains classes offering console commands

### UnitConversionTool-currency
Containis additional converters for converting few currencies

### UnitConversionTool-service
Contains concrete UnitConvertingService and some basic convertors. 

### UnitConversionTool-servlet
Contains web user interface in form of simple servlet
