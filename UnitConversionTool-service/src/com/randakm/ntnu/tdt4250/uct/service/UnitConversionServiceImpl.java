package com.randakm.ntnu.tdt4250.uct.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

import com.randakm.ntnu.tdt4250.uct.api.Unit;
import com.randakm.ntnu.tdt4250.uct.api.UnitConversionService;
import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;

@Component
public class UnitConversionServiceImpl implements UnitConversionService {
	private Map<String, UnitConvertor> convertorsMap = new HashMap<>();
	
	@Reference(cardinality = ReferenceCardinality.OPTIONAL)
	private volatile LoggerFactory loggerFactory;
	
	private Logger getLogger() {
		if (loggerFactory != null) {
			return loggerFactory.getLogger(UnitConversionServiceImpl.class);
		}
		return null;
	}
	
	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			bind = "addConvertor",
			unbind = "removeConvertor"
	)
	public void addConvertor(UnitConvertor conv) {
		getLogger().info("added converter ");
		convertorsMap.put(createConversionKey(conv), conv);
	}
	
	public void removeConvertor(UnitConvertor conv) {
		boolean removed = convertorsMap.remove(createConversionKey(conv), conv);
		if(removed) getLogger().info("removed converter "+conv);
	}
	
	@Override
	public BigDecimal convert(Unit source, Unit target, BigDecimal amount) {
		getLogger().info("Convert "+amount+" of "+source.getUnitKey()+" to "+target.getUnitKey());
		
		if(source.equals(target)) return amount;
		
		String key = createConversionKey(source, target);
		
		UnitConvertor uc = convertorsMap.get(key);
		if(uc != null) return uc.convert(amount);
		
		
		throw new MissingResourceException(
				"No usable convetor found for "+key, 
				UnitConvertor.class.getName(), 
				key
				);
	}

	private String createConversionKey(UnitConvertor uc) {
		return createConversionKey(uc.getSourceUnitKey(), uc.getTargetUnitKey());
	}
	private String createConversionKey(Unit s, Unit t) {
		return createConversionKey(s.getUnitKey(), t.getUnitKey());
	}
	private String createConversionKey(String sourceUnitKey, String targetUnitKey) {
		return sourceUnitKey+"->"+targetUnitKey;
	}
	

}
