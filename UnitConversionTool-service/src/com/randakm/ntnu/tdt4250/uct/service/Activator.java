package com.randakm.ntnu.tdt4250.uct.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;

public class Activator implements BundleActivator {
	protected static final BigDecimal C_TO_K = new BigDecimal("273.15");
	
	private List<ServiceRegistration<UnitConvertor>> regs = new ArrayList<>();
	
	@Override
	public void start(BundleContext context) throws Exception {
		List<UnitConvertor> list = new ArrayList<>();
		list.add(new CelsiaFahrnConvertor());
		list.add(new FahrnCelsiaConvertor());
		list.add(new CelsiaKelvinConvertor());
		list.add(new KelvinCelsiaConvertor());
		
		for(UnitConvertor uc : list) {
			regs.add(context.registerService(UnitConvertor.class, uc, null));
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		for(ServiceRegistration<UnitConvertor> sr : regs) {
			context.ungetService(sr.getReference());
		}
		
		regs.clear();
	}
}

class CelsiaFahrnConvertor implements UnitConvertor {

	@Override
	public BigDecimal convert(BigDecimal value) {
		return value
				.subtract(new BigDecimal(32))
				.multiply(new BigDecimal(5))
				.divide(new BigDecimal(9), MathContext.DECIMAL32)
				.round(MathContext.DECIMAL32)
				;
	}

	@Override
	public String getSourceUnitKey() {
		return "C";
	}

	@Override
	public String getTargetUnitKey() {
		return "F";
	}
	
}

class FahrnCelsiaConvertor implements UnitConvertor {

	@Override
	public BigDecimal convert(BigDecimal value) {
		return value
				.multiply(new BigDecimal(9))
				.divide(new BigDecimal(5), MathContext.DECIMAL32)
				.add(new BigDecimal(32))
				.round(MathContext.DECIMAL32)
				;
	}

	@Override
	public String getSourceUnitKey() {
		return "F";
	}

	@Override
	public String getTargetUnitKey() {
		return "C";
	}
	
}

class CelsiaKelvinConvertor implements UnitConvertor {

	@Override
	public BigDecimal convert(BigDecimal value) {
		return value.add(Activator.C_TO_K);
	}

	@Override
	public String getSourceUnitKey() {
		return "C";
	}

	@Override
	public String getTargetUnitKey() {
		return "K";
	}
}

class KelvinCelsiaConvertor implements UnitConvertor {

	@Override
	public BigDecimal convert(BigDecimal value) {
		return value.subtract(Activator.C_TO_K);
	}

	@Override
	public String getSourceUnitKey() {
		return "K";
	}

	@Override
	public String getTargetUnitKey() {
		return "C";
	}
}