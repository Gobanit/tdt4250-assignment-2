package com.randakm.ntnu.tdt4250.uct.currency;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Component;

import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;

@Component
public class Activator implements BundleActivator {

	private List<ServiceRegistration<UnitConvertor>> regs = new ArrayList<>();

	@Override
	public void start(BundleContext context) throws Exception {
		final boolean addAllCombinations = true;
		
		List<UnitConvertor> list = new ArrayList<>();

		final String EUR = "EUR";
		
		Map<String, BigDecimal> ratioMap = getEurRatioMap();
		for(String source : ratioMap.keySet()) {
			BigDecimal sourceRatio = ratioMap.get(source);
			list.add(new CurrencyConvertor(EUR, source, sourceRatio));
			BigDecimal invertedSourceRatio = new BigDecimal(1).divide(sourceRatio, MathContext.DECIMAL128);
			list.add(new CurrencyConvertor(source, EUR, invertedSourceRatio));
			
			if(addAllCombinations) {
				for(String target : ratioMap.keySet()) {
					if(source.equals(target)) continue;
					BigDecimal targetRatio = ratioMap.get(target);
					list.add(new CurrencyConvertor(source, target, 
							invertedSourceRatio.multiply(targetRatio, MathContext.DECIMAL128)));
				}
			}
		}
		
		for(UnitConvertor uc : list) {
			regs.add(context.registerService(UnitConvertor.class, uc, null));
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		for(ServiceRegistration<UnitConvertor> sr : regs) {
			context.ungetService(sr.getReference());
		}
		
		regs.clear();
	}

	private Map<String, BigDecimal> getEurRatioMap() {
		Map<String, BigDecimal> map = new HashMap<>();
		map.put("SKK", new BigDecimal(30.127));
		map.put("CZK", new BigDecimal(25));
		map.put("USD", new BigDecimal(1.5));
		map.put("NOK", new BigDecimal(10));
		return map;
	}
	
}
