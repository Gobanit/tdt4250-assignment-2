package com.randakm.ntnu.tdt4250.uct.currency;

import java.math.BigDecimal;
import java.math.MathContext;

import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;

public class CurrencyConvertor implements UnitConvertor {

	private String sourceCurr;
	private String targetCurr;
	private BigDecimal ratio;
	
	
	public CurrencyConvertor(String sourceCurr, String targetCurr, BigDecimal ratio) {
		this.sourceCurr = sourceCurr;
		this.targetCurr = targetCurr;
		this.ratio = ratio;
	}

	@Override
	public BigDecimal convert(BigDecimal value) {
		return value.multiply(ratio, MathContext.DECIMAL32);
	}

	@Override
	public String getSourceUnitKey() {
		return sourceCurr;
	}

	@Override
	public String getTargetUnitKey() {
		return targetCurr;
	}

}
