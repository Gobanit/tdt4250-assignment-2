package com.randakm.ntnu.tdt4250.uct.console;

import java.math.BigDecimal;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;

/**
 * Generic convertor for linear expression. 
 * There need to be spaces separating unit symbols and operators.
 * Unit symbols must contain alphabetic characters only, valid expression is 
 * for example "F = C * 100 + 20" or "EUR = NOK / 10"
 *
 */
public class GenericLinearConverter implements UnitConvertor {

	private String sourceUnitSymbol;
	private String targetUnitSymbol;
	private String expression;
	
	public GenericLinearConverter(String exp) {
		String[] strs = exp.split("=");
		targetUnitSymbol = strs[0].replace(" ", "");
		expression = strs[1];
		
		strs = strs[1].split(" ");
		for(String s : strs) {
			if(s.matches("[a-zA-Z]+")) {
				sourceUnitSymbol = s;
				break;
			}
		}
	}
	
	@Override
	public BigDecimal convert(BigDecimal value) {
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    String exp = expression.replace(sourceUnitSymbol, value.toPlainString());
	    
	    try {
			return new BigDecimal(engine.eval(exp)+"");
		} catch (ScriptException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getSourceUnitKey() {
		return sourceUnitSymbol;
	}

	@Override
	public String getTargetUnitKey() {
		return targetUnitSymbol;
	}

}
