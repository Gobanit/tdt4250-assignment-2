package com.randakm.ntnu.tdt4250.uct.console;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;

import com.randakm.ntnu.tdt4250.uct.api.Unit;
import com.randakm.ntnu.tdt4250.uct.api.UnitConversionService;
import com.randakm.ntnu.tdt4250.uct.api.UnitConvertor;


@Component(
		service = UnitConversionConsole.class,
		property = {
			"osgi.command.scope=unitConversion",
			"osgi.command.function=unitConverters",
			"osgi.command.function=addUnitConverter",
			"osgi.command.function=convert"
		}
	)
public class UnitConversionConsole {
	
	public void unitConverters() throws InvalidSyntaxException {
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		
		Collection<ServiceReference<UnitConvertor>> refs = bc.getServiceReferences(UnitConvertor.class, null);
		System.out.println("There is "+refs.size()+" converters!");
		
		List<UnitConvertor> list = refs.stream().map((r) -> {
			return bc.getService(r);
		}).sorted((uc1, uc2) -> {
			int sourceComp = uc1.getSourceUnitKey()
					.compareTo(uc2.getSourceUnitKey());
			if(sourceComp != 0) return sourceComp;
			int targetComp = uc1.getTargetUnitKey().compareTo(uc2.getTargetUnitKey());
			if(targetComp != 0) return targetComp;
			
			return uc1.getClass().getName().compareTo(uc2.getClass().getName());
		}).collect(Collectors.toList());
		
		int i = 1;
		for(UnitConvertor uc : list) {
			printConv(i++, uc);
		}
	}
	
	public void addUnitConverter(String expression) {
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		UnitConvertor uc = new GenericLinearConverter(expression);
		bc.registerService(UnitConvertor.class, uc, null);
		System.out.println("Converter added");
	}
	
	public void convert(String source, String target, String amount) {
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		UnitConversionService ucs = bc.getService(bc.getServiceReference(UnitConversionService.class));
		BigDecimal result = ucs.convert(new Unit(source), new Unit(target), new BigDecimal(amount));
		
		System.out.println("Result: "+result);
	}
	
	
	private void printConv(int i, UnitConvertor uc) {
		System.out.println(i+". "+uc.getSourceUnitKey() + " -> " + uc.getTargetUnitKey() + " by class "+uc.getClass().getName());
	}
	
	
	
	
}
	
	