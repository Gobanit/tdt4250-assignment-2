package com.randakm.ntnu.tdt4250.uct.api;

import java.math.BigDecimal;

import org.osgi.annotation.versioning.ProviderType;


@ProviderType
public interface UnitConvertor {
	
	public BigDecimal convert(BigDecimal value);
	public String getSourceUnitKey();
	public String getTargetUnitKey();

}
