package com.randakm.ntnu.tdt4250.uct.api;

import java.math.BigDecimal;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface UnitConversionService {

	BigDecimal convert(Unit source, Unit target, BigDecimal amount);
}
