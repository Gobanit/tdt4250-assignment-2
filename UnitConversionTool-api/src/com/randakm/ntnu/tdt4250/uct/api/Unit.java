package com.randakm.ntnu.tdt4250.uct.api;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public class Unit {

	private String key;
	
	public Unit(String key) {
		this.key = key;
	}
	
	public String getUnitKey() {
		return key;
	}
	
}
